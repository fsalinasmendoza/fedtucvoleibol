/* eslint-disable no-console */

const babelify = require('babelify');
const browserify = require('browserify');
const concat = require('gulp-concat');
const cssmin = require('gulp-cssmin');
const gulp = require('gulp');
const gulpif = require('gulp-if');
const gutil = require('gulp-util');
const notify = require('gulp-notify');
const streamify = require('gulp-streamify');
const source = require('vinyl-source-stream');
const uglify = require('gulp-uglify');
const watchify = require('watchify');

const dependencies = [
  'react',
  'react-dom'
];

function browserifyTask(options) {
  let bundler = browserify({
    entries: ['./client/scripts/main.jsx'],
    extensions: ['.js', '.jsx'],
    debug: options.development,
    cache: {},
    packageCache: {},
    fullPaths: options.development
  }).transform(
    babelify, { presets: ['react', 'es2015', 'stage-2'] }
  );

  dependencies.forEach(dep => (bundler.external(dep)));

  function rebundle() {
    const start = Date.now();
    console.log('Building APP bundle');
    bundler.bundle()
      .on('error', gutil.log)
      .pipe(source('main.js'))
      .pipe(gulpif(!options.development, streamify(uglify())))
      .pipe(gulp.dest('./public/js'))
      .pipe(notify(() =>
        (console.log(`APP bundle built in ${Date.now() - start} ms`)
      )));
  }

  if (options.development) {
    bundler = watchify(bundler, { ignoreWatch: '**/node_modules/**' });
    bundler.on('update', rebundle);
  }

  rebundle();

  const vendorsBundler = browserify({
    debug: options.development,
    require: dependencies
  });

  const start = new Date();
  console.log('Building VENDORS bundle');
  vendorsBundler.bundle()
    .on('error', gutil.log)
    .pipe(source('vendors.js'))
    .pipe(gulpif(!options.development, streamify(uglify())))
    .pipe(gulp.dest('./public/js'))
    .pipe(notify(() => (
      console.log(`VENDORS bundle built in ${Date.now() - start} ms`)
    )));
}

function cssTask(options) {
  function run() {
    const start = new Date();
    console.log('Building CSS bundle');
    gulp.src('./styles/*.css')
      .pipe(concat('main.min.css'))
      .pipe(cssmin())
      .pipe(gulp.dest('./public/styles'))
      .pipe(notify(() => (
        console.log(`CSS bundle built in ${Date.now() - start} ms`)
      )));
  }
  run();

  if (options.development) {
    gulp.watch('./styles/*.css', run);
  }
}

gulp.task('deploy', () => {
  process.env.NODE_ENV = 'production';

  browserifyTask({ development: false });

  cssTask({ development: false });
});

gulp.task('default', () => {
  browserifyTask({ development: true });

  cssTask({ development: true });
});
