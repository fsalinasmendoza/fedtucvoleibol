# Requisitos

* nodejs 6+
* mongodb 3.4+
* gulp 3.9+

# Instalación

Instalar las dependencias:

```bash
> npm install
```

Crear archivo .env con las siguientes variables:

* COOKIE_SECRET
* CLOUDINARY_URL
* NODE_ENV (production|development)

## Parche de idioma para Admin UI.

En el directorio del proyecto:

```
cp keystone.patch node_modules
cd node_modules
patch -s -p0 < keystone.patch
rm keystone.patch
```

## Compilación

En el directorio del proyecto:

```bash
> gulp deploy
```


# Ejecución

```bash
> service mongod start
> npm start
```

