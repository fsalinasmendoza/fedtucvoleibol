/* eslint-disable no-console */
const keystone = require('keystone');

const Story = keystone.list('Story');

/**
 * List Stories
 */
exports.list = (req, res) => {
  const filter = { state: 'published' };

  Story.model.find(filter)
    .select('title brief photo publishedAt attachment')
    .limit(12)
    .sort('-publishedAt')
    .lean()
    .exec((err, stories) => {
      if (err) {
        res.reportError('No se puede acceder al listado de noticias.', 500, err);
      } else {
        res.json(stories);
      }
    });
};

/**
 * Get Story
 */
exports.get = (req, res) => {
  Story.model.findById(req.params.id)
    .select('title content image publishedAt attachment')
    .exec((err, item) => {
      if (err) {
        res.reportError('No se puede acceder a la noticia solicitada.', 500, err);
      } else if (!item) {
        res.reportError('Noticia inexistente', 404, new Error(`Couldn't find story ${req.params.id}.`));
      } else {
        res.json(item);
      }
    });
};
