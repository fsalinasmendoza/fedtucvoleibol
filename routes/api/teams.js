/* eslint-disable no-console */
const keystone = require('keystone');

const Team = keystone.list('Team');

/**
 * List Teams
 */
exports.list = (req, res) => {
  Team.model.find({ enabled: true })
    .sort('name')
    .select('name address logo')
    .lean()
    .exec((err, teams) => {
      if (err) {
        res.reportError('No se pudo acceder a los datos de los equipos.', 500, err);
      } else {
        res.json(teams);
      }
    });
};
