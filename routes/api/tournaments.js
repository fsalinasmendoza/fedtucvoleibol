const keystone = require('keystone');

const Tournament = keystone.list('Tournament');

/**
 * Get active tournament with its phases.
 */
exports.getActive = (req, res) => {
  Tournament.model.findOne({ isActive: true }).exec((tournamentErr, tournament) => {
    if (tournamentErr) {
      res.reportError('No se pudo acceder a los datos del torneo.', 500, tournamentErr);
    } else if (tournament == null) {
      res.reportError('No hay torneos activos.', 404);
    } else {
      const tournamentObj = tournament.toObject();
      tournamentObj.phases = [];

      tournament.populateRelated('phases', (phaseErr) => {
        if (phaseErr) {
          res.reportError('No se pudo acceder a las fases del torneo ', 500, phaseErr);
        } else {
          tournamentObj.phases = tournament.phases.reduce((filtered, phase) => {
            if (phase.enabled) {
              filtered.push({ name: phase.name, _id: phase._id, sortOrder: phase.sortOrder });
            }
            return filtered;
          }, []);
          res.json(tournamentObj);
        }
      });
    }
  });
};
