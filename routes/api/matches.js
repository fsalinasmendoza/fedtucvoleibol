/* eslint-disable no-console */
const keystone = require('keystone');

const Match = keystone.list('Match');

/**
 * List Matches
 */
exports.list = (req, res) => {
  const { ageGroup, branch, phase, team } = req.query;
  const filter = { tournament: req.params.tournament };

  Object.assign(
    filter,
    ageGroup && { ageGroup: ageGroup.toString() },
    branch && { branch: branch.toString() },
    phase && { phase: phase.toString() },
    team && { $or: [{ home: team.toString() }, { visit: team.toString() }] }
  );

  Match.model.find(filter)
    .sort('-datetime')
    .lean()
    .limit(48)
    .populate({ path: 'home', select: 'name logo' })
    .populate({ path: 'visit', select: 'name logo' })
    .populate({ path: 'stadium', select: 'name address -_id' })
    .populate({ path: 'firstReferee', select: 'name -_id' })
    .populate({ path: 'secondReferee', select: 'name -_id' })
    .populate({ path: 'scorekeeper', select: 'name -_id' })
    .populate({ path: 'ageGroup', select: 'name' })
    .populate({ path: 'phase', select: 'name sortOrder' })
    .exec((err, matches) => {
      if (err) {
        res.reportError('No se puede acceder al listado de partidos.', 500, err);
      } else {
        res.json(matches);
      }
    });
};
