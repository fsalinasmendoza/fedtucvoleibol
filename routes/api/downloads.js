/* eslint-disable no-console */
const keystone = require('keystone');

const Download = keystone.list('Download');

/**
 * List Downloads
 */
exports.list = (req, res) => {
  /* Exclude downloads with no file bound. */
  const filter = { 'file.path': { $nin: ['', null] } };
  const { category } = req.query;

  Object.assign(filter, category && { category: category.toString() });

  Download.model.find(filter)
    .select('name category file updatedAt')
    .sort('-updatedAt')
    .lean()
    .exec((err, downloads) => {
      if (err) {
        res.reportError('No se puede acceder al listado de descargas.', 500, err);
      } else {
        res.json(downloads);
      }
    });
};
