/* eslint-disable no-console */
const keystone = require('keystone');

const AgeGroup = keystone.list('AgeGroup');

/**
 * List Teams
 */
exports.list = (req, res) => {
  AgeGroup.model.find({ enabled: true }).select('name').lean().exec((err, ageGroups) => {
    if (err) {
      res.reportError('No se pudo acceder a los datos de las categorías.', 500, err);
    } else {
      res.json(ageGroups);
    }
  });
};
