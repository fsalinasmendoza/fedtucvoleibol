const keystone = require('keystone');
const extendResponseCapabilities = require('../middlewares/extendResponseCapabilities');

const importRoutes = keystone.importer(__dirname);

const routes = {
  api: importRoutes('./api')
};

module.exports = (app) => {
  app.use(extendResponseCapabilities);

  app.get('/api/ageGroups', routes.api.ageGroups.list);
  app.get('/api/stories/:id', routes.api.stories.get);
  app.get('/api/stories', routes.api.stories.list);
  app.get('/api/teams', routes.api.teams.list);

  app.get('/api/tournaments/:tournament/matches', routes.api.matches.list);
  app.get('/api/tournaments/active', routes.api.tournaments.getActive);

  app.get('/api/downloads', routes.api.downloads.list);

  app.get('/api/*', (req, res) => res.status(404).json({ message: 'El recurso no existe' }));

  // Views
  app.use((req, res) => {
    res.render('index');
  });
};
