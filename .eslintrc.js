module.exports = {
  "env": {
    "browser": true,
    "node": true
  },
  "parser": "babel-eslint",
  "parserOptions": {
      "ecmaVersion": 7,
      "sourceType": "module",
      "ecmaFeatures": {
          "jsx": true,
        "experimentalObjectRestSpread": true
      }
  },
    "extends": "airbnb",
  "plugins": [
    "react"
  ],
  "rules": {
    "comma-dangle" : 0,
    "no-underscore-dangle": 0,
    "require-jsdoc": 1,
    "react/jsx-uses-vars": 1,
    "react/jsx-uses-react": 1,
        "react/react-in-jsx-scope": 1
  },
  "settings": {
    "react": {
      "pragma": "React",
      "version": "0.14.8"
    }
  },
};
