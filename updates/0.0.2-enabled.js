const keystone = require('keystone');

const AgeGroup = keystone.list('AgeGroup');
const Phase = keystone.list('Phase');
const Referee = keystone.list('Referee');
const Scorekeeper = keystone.list('Scorekeeper');
const Team = keystone.list('Team');

/**
 * Sets the enabled field in each document to true.
 */
function addEnabledField(models, done) {
  if (models.length !== 0) {
    const model = models.pop();
    model.update({}, { enabled: true }, { multi: true }, (err) => {
      if (err) {
        done(err);
      } else {
        addEnabledField(models, done);
      }
    });
  } else {
    done();
  }
}

module.exports = (done) => {
  const models = [AgeGroup.model, Phase.model, Referee.model, Scorekeeper.model, Team.model];

  addEnabledField(models, done);
};
exports = module.exports;
