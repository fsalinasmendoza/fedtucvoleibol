/* eslint-disable no-console, no-param-reassign */

/**
 * Middleware that adds functions related with the API to the res object.
 */
module.exports = function extendResponseCapabilities(req, res, next) {
  res.reportError = function reportError(message, status, cause) {
    console.error(`Error ${status}: ${message}`);
    if (cause) {
      console.error(`    Cause: ${cause.message}`);

      const errorObject = {};
      Error.captureStackTrace(errorObject);

      /* Get the first three lines of the stack trace. */
      const stackLines = errorObject.stack.split('\n', 3);
      /* The third one contains the file name and the line number from which the error reporter was
       * called. */
      if (stackLines[2]) {
        console.error(`    ${stackLines[2]}`);
      }
    }
    res.status(status);
    res.json({ message });
  };

  next();
};
