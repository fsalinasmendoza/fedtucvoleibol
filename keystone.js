require('dotenv').config();
const keystone = require('keystone');

keystone.init({
  name: 'fedtucvoleibol',
  brand: 'fedtucvoleibol',

  less: 'public',
  static: 'public',
  favicon: 'public/favicon.ico',
  views: 'templates/views',
  'view engine': 'jade',

  'auto update': true,
  session: true,
  'session store': 'mongo',
  auth: true,
  'user model': 'User',
  'cookie secret': '21a29742c0e53524665c2828c9f136dbbc5a086297da6dbe0f354e4344119aa8c81ebec9d2e71a1d7615d6e1039ffa16e0e38e7e7e5737b2c2b686bfeda6b9d6',
});

keystone.import('models');

keystone.set('locals', {
  _: require('underscore'),
  env: keystone.get('env'),
  utils: keystone.utils,
  editable: keystone.content.editable
});

keystone.set('routes', require('./routes'));

keystone.set('nav', {
  noticias: 'stories',
  campeonatos: ['matches', 'phases', 'teams', 'tournaments', 'age-groups'],
  personas: ['referees', 'scorekeepers'],
  institucional: ['downloads'],
  usuarios: 'users'
});
keystone.start();
