import React from 'react';
import TeamBlock from '../components/TeamBlock';
import { DynamicListType } from '../utils/propTypes';

const TeamPage = ({ teams }) => (
  <TeamBlock teams={teams} />
);

TeamPage.propTypes = {
  teams: DynamicListType.isRequired,
};

TeamPage.defaultProps = {
  teams: { items: null, error: null, loading: false },
};

export default TeamPage;
