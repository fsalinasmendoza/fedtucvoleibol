import React, { PropTypes } from 'react';
import DownloadBlockContainer from '../containers/DownloadBlockContainer';

const DownloadPage = props => (
  <DownloadBlockContainer category={props.params.category} />
);

DownloadPage.propTypes = {
  params: PropTypes.shape({
    category: PropTypes.string.isRequired,
  }).isRequired
};

export default DownloadPage;
