import React from 'react';
import StoryDetailsContainer from '../containers/StoryDetailsContainer';

const StoryPage = props => (
  <StoryDetailsContainer id={props.params.id} />
);
StoryPage.propTypes = {
  params: React.PropTypes.shape({ id: React.PropTypes.string }).isRequired
};

export default StoryPage;
