import React from 'react';
import StoryBlockContainer from '../containers/StoryBlockContainer';

const HomePage = () => (
  <StoryBlockContainer />
);

export default HomePage;
