import React, { PropTypes } from 'react';
import ScheduleContainer from '../containers/ScheduleContainer';

const SchedulePage = ({ teams }) => (
  <ScheduleContainer teams={teams} />
);

SchedulePage.propTypes = {
  teams: PropTypes.shape({
    teams: PropTypes.array,
    loading: PropTypes.boolean,
    error: PropTypes.object,
  }).isRequired,
};

SchedulePage.defaultProps = {
  teams: { teams: null, error: null, loading: false },
};

export default SchedulePage;
