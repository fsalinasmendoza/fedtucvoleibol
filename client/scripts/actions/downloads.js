/* eslint-disable require-jsdoc, no-console */

import Fetcher from '../utils/Fetcher';

export const downloadsFetcher = new Fetcher('DOWNLOADS', '/api/downloads');
