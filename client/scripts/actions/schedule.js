/* eslint-disable require-jsdoc */

import Fetcher from '../utils/Fetcher';

export const matchFetcher = new Fetcher('MATCHES');
export const tournamentFetcher = new Fetcher('TOURNAMENT', '/api/tournaments/active');
export * from './schedule/filter';

export function fetchMatches() {
  return (dispatch, getState) => {
    const scheduleState = getState().schedule;

    if (scheduleState.tournament.tournament) {
      dispatch(
        matchFetcher.fetch(
          scheduleState.filter,
          `/api/tournaments/${scheduleState.tournament.tournament._id}/matches`
        )
      );
    } else {
      dispatch(matchFetcher.reset());
    }
  };
}
