import Fetcher from '../utils/Fetcher';

export const ageGroupFetcher = new Fetcher('AGE_GROUPS', '/api/ageGroups');
