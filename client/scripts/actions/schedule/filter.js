/* eslint-disable require-jsdoc */

export const SET_AGE_GROUP_FILTER = 'SET_AGE_GROUP_FILTER';
export const SET_BRANCH_FILTER = 'SET_BRANCH_FILTER';
export const SET_PHASE_FILTER = 'SET_PHASE_FILTER';
export const SET_TEAM_FILTER = 'SET_TEAM_FILTER';

export const setAgeGroupFilter = filter => ({ type: SET_AGE_GROUP_FILTER, filter });
export const setBranchFilter = filter => ({ type: SET_BRANCH_FILTER, filter });
export const setPhaseFilter = filter => ({ type: SET_PHASE_FILTER, filter });
export const setTeamFilter = filter => ({ type: SET_TEAM_FILTER, filter });
