/* eslint-disable require-jsdoc, no-console */

import Fetcher from '../utils/Fetcher';

export const storiesFetcher = new Fetcher('STORIES', '/api/stories');
export const storyFetcher = new Fetcher('STORY', '/api/stories');
