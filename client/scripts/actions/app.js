/* eslint-disable import/prefer-default-export */

import Fetcher from '../utils/Fetcher';

export const teamFetcher = new Fetcher('TEAMS', '/api/teams');
