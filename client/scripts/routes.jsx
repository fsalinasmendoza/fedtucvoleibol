import React from 'react';
import { IndexRoute, Route } from 'react-router';

import AppContainer from './containers/AppContainer';
import DownloadPage from './pages/DownloadPage';
import HomePage from './pages/HomePage';
import SchedulePage from './pages/SchedulePage';
import StoryPage from './pages/StoryPage';
import TeamsPage from './pages/TeamsPage';


export default (
  <Route path="/" component={AppContainer}>
    <IndexRoute component={HomePage} />
    <Route path="stories/:id" component={StoryPage} />
    <Route path="teams" component={TeamsPage} />
    <Route path="schedule" component={SchedulePage} />
    <Route path="governance/:category" component={DownloadPage} />
  </Route>
);
