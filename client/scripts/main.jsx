import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { browserHistory, Router } from 'react-router';
import { addLocaleData, IntlProvider } from 'react-intl';
import ReactGA from 'react-ga';

import es from 'react-intl/locale-data/es';

import configureStore from './store/configureStore';

import routes from './routes';

addLocaleData(es);

const store = configureStore();


ReactGA.initialize('UA-103269063-1');

/**
 * Inform route changes to google analytics.
 */
function logPageView() {
  ReactGA.set({ page: window.location.pathname + window.location.search });
  ReactGA.pageview(window.location.pathname + window.location.search);
}

ReactDOM.render(
  <Provider store={store}>
    <IntlProvider locale="es">
      <Router history={browserHistory} routes={routes} onUpdate={logPageView} />
    </IntlProvider>
  </Provider>,
  document.getElementById('root')
);
