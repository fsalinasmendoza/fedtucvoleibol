import axios from 'axios';
import querystring from 'query-string';

export default class Fetcher {
  constructor(entityName, url) {
    this.url = url;

    this.fetchAction = `FETCH_${entityName}`;
    this.successAction = `FETCH_${entityName}_SUCCESS`;
    this.failureAction = `FETCH_${entityName}_FAILURE`;
    this.resetAction = `RESET_${entityName}`;

    this.successActionCreator = null;
    this.errorActionCreator = null;
  }

  onSuccess(actionCreator) {
    this.successActionCreator = actionCreator;
    return this;
  }

  onFailure(actionCreator) {
    this.errorActionCreator = actionCreator;
    return this;
  }

  fetchOne(id, url) {
    return (dispatch) => {
      dispatch(this.requestData());

      return axios({
        url: `${url || this.url}/${id}`,
        headers: []
      })
      .then((response) => {
        dispatch(this.receiveData(response.data));

        if (this.successActionCreator) {
          dispatch(this.successActionCreator(response.data));
        }
      })
      .catch((error) => {
        const json = error.response ? error.response.data : error;
        dispatch(this.receiveError(json));

        if (this.errorActionCreator) {
          dispatch(this.errorActionCreator(json));
        }
      });
    };
  }

  /**
   * Realiza una consulta asincrónica al web service.
   *
   * @param {object} query - Objeto a partir del cual se crea un query string.
   * @param {string} [url] - URL a la que se realizará la consulta. Si se omite, se usa la URL
   * que se definió en el constructor.
   */
  fetch(query, url) {
    return (dispatch) => {
      dispatch(this.requestData());

      return axios({
        url: query ? `${url || this.url}?${querystring.stringify(query)}` : url || this.url,
        headers: []
      })
      .then((response) => {
        dispatch(this.receiveData(response.data));

        if (this.successActionCreator) {
          dispatch(this.successActionCreator(response.data));
        }
      })
      .catch((error) => {
        const json = error.response ? error.response.data : error;
        dispatch(this.receiveError(json));

        if (this.errorActionCreator) {
          dispatch(this.errorActionCreator(json));
        }
      });
    };
  }

  receiveData(json) {
    return {
      type: this.successAction,
      payload: json
    };
  }

  receiveError(json) {
    return {
      type: this.failureAction,
      payload: json
    };
  }

  requestData() {
    return {
      type: this.fetchAction
    };
  }

  reset() {
    return {
      type: this.resetAction
    };
  }
}
