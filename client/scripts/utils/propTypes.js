/* eslint-disable import/prefer-default-export */
import { PropTypes } from 'react';

export const DynamicListType = PropTypes.shape({
  items: PropTypes.array,
  loading: PropTypes.bool,
  error: PropTypes.shape({
    message: PropTypes.string
  })
});
