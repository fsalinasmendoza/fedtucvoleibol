/* eslint-disable import/prefer-default-export */

export const groupBy = (array, index) => {
  const getIndex = typeof index === 'string' ? (obj => obj[index]) : index;

  return array.reduce((accumulator, object) => {
    const indexValue = getIndex(object);
    if (indexValue in accumulator) {
      accumulator[indexValue].push(object);
    } else {
      Object.assign(accumulator, { [indexValue]: [object] });
    }
    return accumulator;
  },
  {});
};
