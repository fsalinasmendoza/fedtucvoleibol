import { connect } from 'react-redux';
import { downloadsFetcher } from '../actions/downloads';
import DownloadBlock from '../components/DownloadBlock';


const mapStateToProps = (globalState, ownProps) => (
  {
    downloads: globalState.downloads.downloads,
    category: ownProps.category
  }
);

const mapDispatchToProps = dispatch => (
  {
    fetchDownloads: (category) => {
      dispatch(downloadsFetcher.fetch(category));
    }
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(DownloadBlock);
