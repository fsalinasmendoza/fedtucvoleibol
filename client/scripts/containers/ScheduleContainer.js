import { connect } from 'react-redux';
import {
  tournamentFetcher,
  fetchMatches,

  setAgeGroupFilter,
  setBranchFilter,
  setPhaseFilter,
  setTeamFilter,
} from '../actions/schedule';
import Schedule from '../components/Schedule';

const mapStateToProps = (globalState, ownProps) => (
  {
    tournament: globalState.schedule.tournament,
    matches: globalState.schedule.matches,

    filter: globalState.schedule.filter,

    teams: ownProps.teams,
  }
);

const mapDispatchToProps = dispatch => (
  {
    fetchActiveTournament: () => {
      dispatch(tournamentFetcher.fetch());
    },
    fetchMatches: (tournament, filter) => {
      dispatch(fetchMatches(tournament, filter));
    },
    setAgeGroupFilter: (filter) => {
      dispatch(setAgeGroupFilter(filter));
    },
    setBranchFilter: (filter) => {
      dispatch(setBranchFilter(filter));
    },
    setPhaseFilter: (filter) => {
      dispatch(setPhaseFilter(filter));
    },
    setTeamFilter: (filter) => {
      dispatch(setTeamFilter(filter));
    },
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(Schedule);
