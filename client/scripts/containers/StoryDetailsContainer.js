import { connect } from 'react-redux';
import { storyFetcher } from '../actions/stories';
import StoryDetails from '../components/StoryDetails';

const mapStateToProps = (globalState, ownProps) => (
  {
    activeStory: globalState.stories.activeStory,
    storyId: ownProps.id
  }
);

const mapDispatchToProps = dispatch => (
  {
    fetchStory: (storyId) => {
      dispatch(storyFetcher.fetchOne(storyId));
    },
    resetStory: () => {
      dispatch(storyFetcher.reset());
    }
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(StoryDetails);
