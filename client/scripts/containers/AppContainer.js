import { connect } from 'react-redux';
import { teamFetcher } from '../actions/app';
import App from '../components/App';


const mapStateToProps = state => (
  {
    teams: state.app.teams,
  }
);

const mapDispatchToProps = dispatch => (
  {
    fetchTeams: () => {
      dispatch(teamFetcher.fetch());
    }
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(App);
