import { connect } from 'react-redux';
import MatchFilter from '../components/MatchFilter';
import { ageGroupFetcher } from '../actions/matchFilter';


const mapStateToProps = (globalState, ownProps) => (
  {
    ageGroups: globalState.matchFilter.ageGroups,
    ageGroupFilter: ownProps.ageGroupFilter,
    setAgeGroupFilter: ownProps.setAgeGroupFilter,

    branchFilter: ownProps.branchFilter,
    setBranchFilter: ownProps.setBranchFilter,

    phases: ownProps.phases,
    phaseFilter: ownProps.phaseFilter,
    setPhaseFilter: ownProps.setPhaseFilter,

    teams: ownProps.teams,
    teamFilter: ownProps.teamFilter,
    setTeamFilter: ownProps.setTeamFilter,
  }
);

const mapDispatchToProps = dispatch => (
  {
    fetchAgeGroups: () => {
      dispatch(ageGroupFetcher.fetch());
    },
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(MatchFilter);
