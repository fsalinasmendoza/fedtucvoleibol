import { connect } from 'react-redux';
import { storiesFetcher } from '../actions/stories';
import StoryBlock from '../components/StoryBlock';


const mapStateToProps = globalState => (
  {
    stories: globalState.stories.stories,
  }
);

const mapDispatchToProps = dispatch => (
  {
    fetchStories: () => {
      dispatch(storiesFetcher.fetch());
    }
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(StoryBlock);
