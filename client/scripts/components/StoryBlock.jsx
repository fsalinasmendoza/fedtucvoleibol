import React, { Component, PropTypes } from 'react';
import { Card, Loader, Message } from 'semantic-ui-react';
import StoryPreview from '../components/StoryPreview';
import { DynamicListType } from '../utils/propTypes';


class StoryBlock extends Component {

  componentDidMount() {
    this.props.fetchStories();
  }

  render() {
    const { items, loading, error } = this.props.stories;
    if (loading) {
      return <Loader active inline="centered" />;
    } else if (error) {
      return (
        <Message negative header={error.message} />
      );
    } else if (items == null) {
      return null;
    } else if (items.length === 0) {
      return (
        <Message info header="No hay nada para mostrar." />
      );
    }

    return (
      <Card.Group stackable itemsPerRow={3}>
        {
          items.map(
            story => (<StoryPreview story={story} key={story._id} />)
          )
        }
      </Card.Group>
    );
  }
}

StoryBlock.propTypes = {
  fetchStories: PropTypes.func.isRequired,
  stories: DynamicListType.isRequired,
};

export default StoryBlock;
