import React, { PropTypes } from 'react';
import Diacritics from 'diacritics';
import { Divider, Grid, Header, Icon, Image, List, Segment } from 'semantic-ui-react';

const segmentStyle = {
  padding: '0',
  clear: 'left',
};

const renderStaffMember = (member, label) => (
  <List.Item>
    <b>{ label }: </b>
    {member ? `${member.name.first} ${member.name.last}` : 'A definir'}
  </List.Item>
);

const renderLogo = (logo) => {
  if (logo) {
    return <Image size="mini" src={logo} verticalAlign="middle" spaced />;
  }
  return (<div
    style={{
      float: 'left',
      width: '35px',
      height: '35px',
      marginLeft: '5.5px',
      marginRight: '5.5px'
    }}
  />);
};

const Match = ({ match, first }) => {
  let location = null;
  const locationText = `Estadio ${match.stadium.name}`;
  const address = match.stadium.address;
  if (address && (address.geo || (address.street1 && address.suburb && address.state))) {
    let locationUrl = null;

    if (address.geo) {
      locationUrl = `https://www.google.com/maps/place/${address.geo[1]},${address.geo[0]}`;
    } else {
      locationUrl = Diacritics.remove(
        `https://maps.google.com.ar?q=${address.street1} ${address.suburb} ${address.state}`
      );
    }
    location = (
      <a style={{ color: '#999999' }} href={locationUrl}>
        {locationText}
        <Icon name="marker" />
      </a>
    );
  } else {
    location = <span style={{ color: '#999999' }}>{locationText}</span>;
  }

  return (
    <div>
      {first ? null : <Divider />}
      <Grid verticalAlign="middle">
        <Grid.Column mobile={16} tablet={16} computer={2}>
          <span style={{ fontWeight: 800, marginRight: '2em' }}> {`${match.time} hs `} </span> {location}
        </Grid.Column>
        <Grid.Column mobile={16} tablet={8} computer={7} textAlign="left">
          <Segment basic style={segmentStyle}>
            {renderLogo(match.home.logo)}
            <span style={{ fontSize: '1.2em', fontWeight: '800' }}>{match.home.name}</span>
            <Header as="h2" floated="right">{match.homeScore}</Header>
          </Segment>
          <Segment basic style={segmentStyle}>
            {renderLogo(match.visit.logo)}
            <span style={{ fontSize: '1.2em', fontWeight: '800' }}>{match.visit.name}</span>
            <Header as="h2" floated="right">{match.visitScore}</Header>
          </Segment>
        </Grid.Column>
        <Grid.Column mobile={16} tablet={3} computer={2}>
          <Header as="h5">
            Categoría <span style={{ whiteSpace: 'nowrap' }}>
              {match.ageGroup.name}
            </span> <span style={{ whiteSpace: 'nowrap' }}>
              {match.branch}
            </span>
          </Header>
        </Grid.Column>
        <Grid.Column mobile={16} tablet={5} computer={5}>
          <List>
            {renderStaffMember(match.firstReferee, '1er árbitro')}
            {renderStaffMember(match.secondReferee, '2do árbitro')}
            {renderStaffMember(match.scorekeeper, 'Planillero')}
          </List>
        </Grid.Column>
      </Grid>
    </div>
  );
};

Match.propTypes = {
  match: PropTypes.shape({
    home: PropTypes.object.isRequired,
    visit: PropTypes.object.isRequired,
    stadium: PropTypes.object.isRequired,
    date: PropTypes.string.isRequired,
    time: PropTypes.string.isRequired,
    branch: PropTypes.string.isRequired,
    ageGroup: PropTypes.object.isRequired,
  }).isRequired,
  first: PropTypes.bool.isRequired,
};

export default Match;
