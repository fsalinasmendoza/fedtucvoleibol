import React, { PropTypes } from 'react';
import { Card, Header, Icon } from 'semantic-ui-react';
import { FormattedRelative } from 'react-intl';

const DownloadItem = ({ download }) => {
  const isPdf = download.file.filetype.search('pdf') >= 0;
  const color = isPdf ? 'red' : 'green';
  return (
    <Card color={color} href={`/download/${download.file.filename}`}>
      <Card.Content>
        <Header as="h3" icon>
          <Icon name={`file ${isPdf ? 'pdf' : 'excel'} outline`} size="huge" color={color} />
          { download.name }
        </Header>
      </Card.Content>
      <Card.Content extra>
        <span className="date">
          Actualizado <FormattedRelative
            value={new Date(download.updatedAt)}
          />
        </span>
      </Card.Content>
    </Card>
  );
};

DownloadItem.propTypes = {
  download: PropTypes.shape({
    name: PropTypes.string.isRequired,
    updatedAt: PropTypes.string.isRequired,
    file: PropTypes.shape({
      filename: PropTypes.string.isRequired,
      filetype: PropTypes.string.isRequired,
    })
  }).isRequired
};

export default DownloadItem;
