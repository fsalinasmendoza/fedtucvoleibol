import React, { PropTypes } from 'react';
import { Grid, Icon, Image, Header } from 'semantic-ui-react';
import Diacritics from 'diacritics';

const renderAddress = (address) => {
  if (address == null) {
    return null;
  }

  let locationUrl = null;
  if (address.geo) {
    locationUrl = `https://www.google.com/maps/place/${address.geo[1]},${address.geo[0]}`;
  } else if (address.street1 && address.suburb && address.state) {
    locationUrl = Diacritics.remove(
      `https://maps.google.com.ar?q=${address.street1} ${address.suburb} ${address.state}`
    );
  }

  if (locationUrl) {
    return (
      <Header.Subheader>
        <a style={{ color: 'gray' }} href={locationUrl}>
          {address.street1}<br />{address.suburb} <Icon name="marker" />
        </a>
      </Header.Subheader>
    );
  }

  return (
    <Header.Subheader>
      <span style={{ color: 'gray' }} >{address.street1}<br />{address.suburb}</span>
    </Header.Subheader>
  );
};

const renderLogo = (logo) => {
  if (logo) {
    return <Image size="tiny" src={logo} verticalAlign="middle" />;
  }

  return (
    <div
      style={{
        width: '80px',
        height: '80px',
        display: 'inline-block',
        verticalAlign: 'middle',
      }}
    />
  );
};

const TeamPreview = ({ team }) => (
  <Grid.Column>
    <Header>
      {renderLogo(team.logo)}
      <Header.Content style={{ verticalAlign: 'middle', paddingLeft: '.75rem' }}>
        {team.name}
        {renderAddress(team.address)}
      </Header.Content>
    </Header>
  </Grid.Column>
);

TeamPreview.propTypes = {
  team: PropTypes.shape({
    name: PropTypes.string,
    logo: PropTypes.string,
    address: PropTypes.object
  }).isRequired
};

export default TeamPreview;
