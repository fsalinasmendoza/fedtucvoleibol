import React from 'react';
import { Grid, Header, Loader, Message, Segment } from 'semantic-ui-react';
import { FormattedDate } from 'react-intl';

import Match from './Match';
import { groupBy } from '../utils/utils';
import { DynamicListType } from '../utils/propTypes';


/**
 * Renderiza un conjunto de partidos agrupándolos por fecha.
 *
 * @param {array} allMatches - Listado de partidos.
 */
function renderGroupedByDay(allMatches) {
  const matchesByDate = groupBy(allMatches, 'date');

  return Object.entries(matchesByDate).map(([date, matchList]) => (
    <Grid.Row key={date} as={Segment}>
      <Grid.Column
        mobile={16}
        tablet={16}
        computer={3}
        largeScreen={3}
        widescreen={2}
        style={{ paddingTop: '1em', paddingBottom: '1em' }}
      >
        <Header as="h3">
          <FormattedDate
            value={new Date(Date.parse(date))}
            day="numeric"
            month="long"
            weekday="long"
          />
        </Header>
      </Grid.Column>
      <Grid.Column
        mobile={16}
        tablet={16}
        computer={13}
        largeScreen={13}
        widescreen={14}
        verticalAlign="middle"
        className="notpadded"
      >
        {
          matchList.map((match, i) => (<Match match={match} key={match._id} first={i === 0} />))
        }
      </Grid.Column>
    </Grid.Row>
  ));
}

/**
 * Renderiza un conjunto de partidos agrupándolos por fase.
 *
 * @param {array} allMatches - Listado de partidos.
 */
function renderGroupedByPhase(allMatches) {
  const matchesByPhase = groupBy(allMatches, obj => obj.phase.name);

  return Object.entries(matchesByPhase)
    .sort((a, b) => b[1][0].phase.sortOrder - a[1][0].phase.sortOrder)
    .map(([phase, matches]) => (
      [
        <Header as="h2" style={{ marginTop: '1em' }}>{ phase }</Header>,
        renderGroupedByDay(matches)
      ]
    )
  );
}

const MatchList = (props) => {
  const { items, loading, error } = props.matches;

  if (loading) {
    return <Loader active inline="centered" />;
  } else if (error) {
    return <Message negative header={error.message} />;
  } else if (items == null) {
    return null;
  } else if (items.length === 0) {
    return <Message info header="No hay partidos para mostrar." />;
  }

  return (
    <Grid container style={{ margin: 0 }}>
      { renderGroupedByPhase(items) }
    </Grid>
  );
};

MatchList.propTypes = {
  matches: DynamicListType.isRequired,
};

export default MatchList;
