import React, { Component, PropTypes } from 'react';
import { Card, Header, Loader, Message, Segment } from 'semantic-ui-react';
import DownloadItem from '../components/DownloadItem';
import { DynamicListType } from '../utils/propTypes';


const CATEGORIES = {
  resolutions: 'Resoluciones',
  regulations: 'Reglamentos',
  forms: 'Formularios',
  resources: 'Recursos'
};

class DownloadBlock extends Component {

  componentDidMount() {
    this.props.fetchDownloads({ category: this.props.category });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.category !== this.props.category) {
      this.props.fetchDownloads({ category: nextProps.category });
    }
  }

  render() {
    const { items, loading, error } = this.props.downloads;
    let content = null;

    if (loading) {
      content = <Loader active inline="centered" />;
    } else if (error) {
      content = <Message negative header={error.message} />;
    } else if (items == null) {
      content = null;
    } else if (items.length === 0) {
      content = <Message info header="No hay nada para mostrar." />;
    } else {
      content = (
        <Segment>
          <Card.Group itemsPerRow={5} doubling >
            {
              items.map(
                download => (<DownloadItem download={download} key={download.name} />)
              )
            }
          </Card.Group>
        </Segment>
      );
    }

    return (
      <div>
        <Header as="h1">{CATEGORIES[this.props.category]}</Header>
        {content}
      </div>
    );
  }
}

DownloadBlock.propTypes = {
  fetchDownloads: PropTypes.func.isRequired,
  downloads: DynamicListType.isRequired,
  category: PropTypes.string.isRequired
};

export default DownloadBlock;
