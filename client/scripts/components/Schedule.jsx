import React, { Component, PropTypes } from 'react';
import { Loader, Message } from 'semantic-ui-react';
import MatchFilterContainer from '../containers/MatchFilterContainer';
import MatchList from './MatchList';
import { DynamicListType } from '../utils/propTypes';


class Schedule extends Component {
  componentDidMount() {
    this.props.fetchActiveTournament();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.tournament !== this.props.tournament || nextProps.filter !== this.props.filter) {
      this.props.fetchMatches(this.props.tournament, this.props.filter);
    }
  }

  render() {
    const { tournament, loading, error } = this.props.tournament;
    const filter = this.props.filter;

    if (loading) {
      return <Loader active inline="centered" />;
    } else if (error) {
      return <Message negative header={error.message} />;
    } else if (tournament == null) {
      return null;
    }

    return (
      <div>
        <MatchFilterContainer
          ageGroupFilter={filter.ageGroup}
          setAgeGroupFilter={this.props.setAgeGroupFilter}

          branchFilter={filter.branch}
          setBranchFilter={this.props.setBranchFilter}

          phases={tournament.phases}
          phaseFilter={filter.phase}
          setPhaseFilter={this.props.setPhaseFilter}

          teams={this.props.teams}
          teamFilter={filter.team}
          setTeamFilter={this.props.setTeamFilter}
        />
        <MatchList
          matches={this.props.matches}
          filter={filter}
        />
      </div>
    );
  }
}

Schedule.propTypes = {
  fetchActiveTournament: PropTypes.func.isRequired,
  fetchMatches: PropTypes.func.isRequired,

  tournament: PropTypes.shape({
    tournament: PropTypes.shape({
      phases: PropTypes.arrayOf(PropTypes.object)
    }),
    loading: PropTypes.bool,
    error: PropTypes.shape({
      message: PropTypes.string
    })
  }).isRequired,

  filter: PropTypes.shape({}).isRequired,
  matches: DynamicListType.isRequired,
  teams: DynamicListType.isRequired,

  setAgeGroupFilter: PropTypes.func.isRequired,
  setBranchFilter: PropTypes.func.isRequired,
  setPhaseFilter: PropTypes.func.isRequired,
  setTeamFilter: PropTypes.func.isRequired,
};

Schedule.defaultProps = {
  filter: {},
};

export default Schedule;
