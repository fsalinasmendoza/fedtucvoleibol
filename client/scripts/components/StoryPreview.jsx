import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { Card, Icon, Image, Label } from 'semantic-ui-react';
import Truncate from 'react-truncate-html';
import { FormattedRelative } from 'react-intl';

const StoryPreview = ({ story }) => {
  let photo = null;
  let brief = null;
  let attachment = null;

  if (story.photo) {
    photo = (
      <Image src={story.photo} as={Link} to={`stories/${story._id}`} />
    );
  } else {
    brief = (
      <Card.Description>
        <Truncate
          lines={10}
          dangerouslySetInnerHTML={{ __html: story.brief }}
        />
      </Card.Description>
    );
  }

  if (story.attachment) {
    attachment = (
      <Card.Content extra>
        <Label
          as="a"
          color="red"
          size="large"
          href={`download/${story.attachment.filename}`}
        >
          <Icon name="file pdf outline" size="large" /> {story.attachment.originalname}
        </Label>
      </Card.Content>
    );
  }

  return (
    <Card color="red">
      {photo}
      <Card.Content>
        <Card.Header>
          <Link to={`stories/${story._id}`} style={{ color: '#333' }}>
            {story.title}
          </Link>
        </Card.Header>
        {brief}
      </Card.Content>
      {attachment}
      <Card.Content extra>
        <span className="date">
          Publicado <FormattedRelative
            value={new Date(story.publishedAt)}
          />
        </span>
      </Card.Content>
    </Card>
  );
};

StoryPreview.propTypes = {
  story: PropTypes.shape({
    title: PropTypes.string,
    brief: PropTypes.string,
    photo: PropTypes.string,
    publishedAt: PropTypes.string
  }).isRequired
};

export default StoryPreview;
