import React from 'react';
import { IndexLink } from 'react-router';
import { Container, Grid, Icon, Segment } from 'semantic-ui-react';

const PageFooter = () => (
  <Container fluid style={{ flexShrink: 0 }}>
    <Segment
      attached
      color="black"
      inverted
      padded
      style={{ color: '#FFFFFF' }}
    >
      <Grid container stackable>
        <Grid.Row columns={3} centered>
          <Grid.Column>
            <IndexLink to="/">
              fedtucvoleibol.com.ar
            </IndexLink>
          </Grid.Column>
          <Grid.Column>
            Av. Colón 471 — P.A. San Miguel de Tucumán
          </Grid.Column>
          <Grid.Column>
            Seguinos:
            <a href="https://www.facebook.com/federacion.devoleibol">
              <Icon name="facebook square" size="large" />
            </a>
            <a href="https://twitter.com/FedTucVoley">
              <Icon name="twitter square" size="large" />
            </a>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row centered>
          <small>Copyright &copy; 2017 FTV. Todos los derechos reservados.</small>
        </Grid.Row>
      </Grid>
    </Segment>
  </Container>
);

export default PageFooter;
