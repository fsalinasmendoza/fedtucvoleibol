import React from 'react';
import { Grid, Loader, Message, Segment } from 'semantic-ui-react';
import TeamPreview from '../components/TeamPreview';
import { DynamicListType } from '../utils/propTypes';


const TeamBlock = (props) => {
  const { items, loading, error } = props.teams;

  if (loading) {
    return (
      <Segment>
        <Loader active />
      </Segment>
    );
  } else if (error) {
    return (
      <Message negative header={error.message} />
    );
  } else if (items == null) {
    return null;
  } else if (items.length === 0) {
    return (
      <Message info header="No hay nada para mostrar." />
    );
  }

  return (
    <Segment>
      <Grid columns={3} stackable doubling >
        {
          items.map(
            team => (<TeamPreview team={team} key={team.name} />)
          )
        }
      </Grid>
    </Segment>
  );
};

TeamBlock.propTypes = {
  teams: DynamicListType.isRequired
};

export default TeamBlock;
