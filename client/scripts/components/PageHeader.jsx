import React from 'react';
import { IndexLink, Link } from 'react-router';
import { Container, Dropdown, Grid, Header, Image, Menu, Segment } from 'semantic-ui-react';

const PageHeader = () => (
  <Container fluid style={{ flexShrink: 0 }}>
    <Segment attached color="yellow" inverted>
      <Grid container>
        <Grid.Row only="tablet mobile">
          <Image as={IndexLink} to="/" src="/img/logo.png" centered />
        </Grid.Row>
        <Grid.Row only="computer">
          <IndexLink to="/">
            <Header
              as="h2"
              image="/img/logo.png"
              color="red"
              content="Federación Tucumana de Voleibol"
            />
          </IndexLink>
        </Grid.Row>
      </Grid>
    </Segment>
    <Menu attached="bottom" color="red" size="huge" inverted borderless>
      <Container>
        <Menu.Item as={Link} to="/">
          Noticias
        </Menu.Item>

        <Dropdown text="Torneo" className="link item">
          <Dropdown.Menu>
            <Dropdown.Item as={Link} to="/schedule">Fixture</Dropdown.Item>
            <Dropdown.Item as={Link} to="/teams">Clubes</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>

        <Dropdown text="Institucional" className="link item">
          <Dropdown.Menu>
            <Dropdown.Item as={Link} to="/governance/regulations">Reglamentos</Dropdown.Item>
            <Dropdown.Item as={Link} to="/governance/forms">Formularios</Dropdown.Item>
            <Dropdown.Item as={Link} to="/governance/resolutions">Resoluciones</Dropdown.Item>
            <Dropdown.Item as={Link} to="/governance/resources">Recursos</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>

      </Container>
    </Menu>
  </Container>
);

export default PageHeader;
