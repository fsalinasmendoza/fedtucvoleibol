import React, { Component, PropTypes } from 'react';
import { Container, Divider } from 'semantic-ui-react';
import PageFooter from './PageFooter';
import PageHeader from './PageHeader';
import { DynamicListType } from '../utils/propTypes';


class App extends Component {
  componentDidMount() {
    this.props.fetchTeams();
  }

  render() {
    return (
      <Container
        fluid
        style={{ display: 'flex', height: '100vh', flexDirection: 'column' }}
      >
        <PageHeader />
        <Divider hidden />
        <Container style={{ flex: '1 0 auto' }} >
          {React.cloneElement(this.props.children, { teams: this.props.teams })}
        </Container>
        <Divider hidden />
        <PageFooter />
      </Container>
    );
  }
}

App.propTypes = {
  fetchTeams: PropTypes.func.isRequired,
  children: PropTypes.element.isRequired,
  teams: DynamicListType.isRequired,
};

export default App;
