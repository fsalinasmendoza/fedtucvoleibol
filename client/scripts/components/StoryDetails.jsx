import React, { Component, PropTypes } from 'react';
import {
  Container,
  Divider,
  Header,
  Icon,
  Image,
  Label,
  Message,
  Segment
} from 'semantic-ui-react';
import { FormattedRelative } from 'react-intl';
import ReactMarkdown from 'react-markdown';

class StoryDetails extends Component {
  static contextTypes = {
    router: PropTypes.object
  };

  componentDidMount() {
    this.props.fetchStory(this.props.storyId);
  }

  componentWillUnmount() {
    this.props.resetStory();
  }

  render() {
    const { story, loading, error } = this.props.activeStory;
    let image = null;
    let attachment = null;

    if (loading) {
      return (
        <Container fluid text>
          <Segment padded="very" loading />
        </Container>
      );
    } else if (error) {
      return <Message negative header={error.message} />;
    } else if (!story) {
      return <span />;
    }

    if (story.image) {
      image = <Image fluid src={story.image.url} />;
    }

    if (story.attachment) {
      attachment = (
        <Label
          as="a"
          color="red"
          size="large"
          href={`download/${story.attachment.filename}`}
        >
          <Icon name="file pdf outline" size="large" /> {story.attachment.originalname}
        </Label>
      );
    }

    return (
      <Container fluid text>
        <Segment padded="very">
          <Header as="h1">
            {story.title}
            <Header.Subheader>
              <small>
                Publicado <FormattedRelative
                  value={new Date(story.publishedAt)}
                />
              </small>
            </Header.Subheader>
          </Header>
          {image}
          <Divider hidden />
          <ReactMarkdown source={story.content.md} />
          <Divider hidden />
          {attachment}
        </Segment>
      </Container>
    );
  }
}

StoryDetails.propTypes = {
  storyId: PropTypes.string.isRequired,
  fetchStory: PropTypes.func.isRequired,
  resetStory: PropTypes.func.isRequired,
  activeStory: PropTypes.shape({
    story: PropTypes.shape({
      title: PropTypes.string.isRequired,
      content: PropTypes.shape(
        { md: PropTypes.string.isRequired }
      ).isRequired,
      publishedAt: PropTypes.string.isRequired
    }),
    loading: PropTypes.bool,
    error: PropTypes.shape({
      message: PropTypes.string
    })
  }).isRequired
};

export default StoryDetails;
