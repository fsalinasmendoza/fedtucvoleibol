import React, { Component, PropTypes } from 'react';
import { Dropdown, Menu } from 'semantic-ui-react';
import { DynamicListType } from '../utils/propTypes';

class MatchFilter extends Component {
  static renderDynamicDropdown(loader, filter, setChange, label, selectAllLabel) {
    if (loader.error) {
      return null;
    } else if (loader.loading) {
      return <Dropdown loading />;
    } else if (loader.items) {
      return MatchFilter.renderDropdown(loader.items, filter, setChange, label, selectAllLabel);
    }

    return null;
  }

  static renderDropdown(items, filter, setChange, label, selectAllLabel) {
    if (items.length === 0) {
      return null;
    }

    const options = selectAllLabel ? [{ text: selectAllLabel, value: undefined, key: 'all' }] : [];
    Array.prototype.push.apply(
      options,
      items.map(item => ({
        text: item.name || item,
        value: item._id || item,
        key: item._id || item,
      }))
    );

    return ([
      <div style={{ position: 'relative' }} key={`${label}Label`}>
        <div
          style={{
            position: 'absolute',
            left: '1.25em',
            top: '0.5em',
            color: 'rgba(0,0,0,.4)',
            textTransform: 'uppercase',
            fontSize: '0.8em',
            letterSpacing: '0.2em',
          }}
        >
          { label }
        </div>
      </div>,
      <Dropdown
        text={filter ? null : selectAllLabel}
        item
        fluid
        onChange={setChange}
        value={filter}
        options={options}
        style={{ paddingTop: '2em', justifyContent: 'initial' }}
        key={`${label}Dropdown`}
      />
    ]);
  }

  componentDidMount() {
    this.props.fetchAgeGroups();
  }

  render() {
    const { phases, phaseFilter, setPhaseFilter } = this.props;
    const phasesDropdown = MatchFilter.renderDropdown(
      phases, phaseFilter, (e, { value }) => setPhaseFilter(value), 'Fase', 'Todas'
    );

    const { teams, teamFilter, setTeamFilter } = this.props;
    const teamsDropdown = MatchFilter.renderDynamicDropdown(
      teams, teamFilter, (e, { value }) => setTeamFilter(value), 'Equipo', 'Todos'
    );

    const { branchFilter, setBranchFilter } = this.props;
    const branchDropdown = MatchFilter.renderDropdown(
      ['Femenino', 'Masculino'], branchFilter, (e, { value }) => setBranchFilter(value), 'Rama', 'Todas'
    );

    const { ageGroups, ageGroupFilter, setAgeGroupFilter } = this.props;
    const ageGroupsDropdown = MatchFilter.renderDynamicDropdown(
      ageGroups, ageGroupFilter, (e, { value }) => setAgeGroupFilter(value), 'Categoría', 'Todas'
    );

    return (
      <Menu stackable widths={4}>
        { phasesDropdown }
        { teamsDropdown }
        { branchDropdown }
        { ageGroupsDropdown }
      </Menu>
    );
  }
}

MatchFilter.propTypes = {
  fetchAgeGroups: PropTypes.func.isRequired,
  setAgeGroupFilter: PropTypes.func.isRequired,
  ageGroupFilter: PropTypes.string,
  ageGroups: DynamicListType.isRequired,

  setBranchFilter: PropTypes.func.isRequired,
  branchFilter: PropTypes.string,

  setPhaseFilter: PropTypes.func.isRequired,
  phaseFilter: PropTypes.string,
  phases: PropTypes.arrayOf(PropTypes.object).isRequired,

  setTeamFilter: PropTypes.func.isRequired,
  teamFilter: PropTypes.string,
  teams: DynamicListType.isRequired,
};

MatchFilter.defaultProps = {
  ageGroupFilter: null,
  branchFilter: null,
  phaseFilter: null,
  teamFilter: null,
};

export default MatchFilter;
