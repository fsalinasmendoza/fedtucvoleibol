/* eslint-disable require-jsdoc */

import { downloadsFetcher } from '../actions/downloads';

const INITIAL_STATE = {
  downloads: { items: null, error: null, loading: false }
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case downloadsFetcher.fetchAction:
      return {
        ...state,
        downloads: { items: null, error: null, loading: true }
      };
    case downloadsFetcher.successAction:
      return {
        ...state,
        downloads: { items: action.payload, error: null, loading: false }
      };
    case downloadsFetcher.failureAction:
      return {
        ...state,
        downloads: { items: null, error: { message: action.payload.message }, loading: false }
      };
    case downloadsFetcher.resetAction:
      return {
        ...state,
        downloads: INITIAL_STATE.downloads,
      };
    default:
      return state;
  }
}
