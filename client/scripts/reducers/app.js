/* eslint-disable require-jsdoc */

import { teamFetcher } from '../actions/app';


const INITIAL_STATE = {
  teams: { items: null, error: null, loading: false }
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {

    case teamFetcher.fetchAction:
      return {
        ...state,
        teams: { items: null, error: null, loading: true }
      };
    case teamFetcher.successAction:
      return {
        ...state,
        teams: { items: action.payload, error: null, loading: false }
      };
    case teamFetcher.failureAction:
      return {
        ...state,
        teams: { items: null, error: { message: action.payload.message }, loading: false }
      };
    case teamFetcher.resetAction:
      return {
        ...state,
        teams: INITIAL_STATE.teams,
      };
    default:
      return state;
  }
}
