/* eslint-disable require-jsdoc */

import { ageGroupFetcher } from '../actions/matchFilter';


const INITIAL_STATE = {
  ageGroups: { items: null, error: null, loading: false },
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case ageGroupFetcher.fetchAction:
      return {
        ...state,
        ageGroups: { items: null, error: null, loading: true }
      };
    case ageGroupFetcher.successAction:
      return {
        ...state,
        ageGroups: { items: action.payload, error: null, loading: false }
      };
    case ageGroupFetcher.failureAction:
      return {
        ...state,
        ageGroups: { items: null, error: { message: action.payload.message }, loading: false }
      };
    case ageGroupFetcher.resetAction:
      return {
        ...state,
        ageGroups: INITIAL_STATE.ageGroups
      };
    default:
      return state;
  }
}
