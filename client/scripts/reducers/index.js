import { combineReducers } from 'redux';
import app from './app';
import downloads from './downloads';
import matchFilter from './matchFilter';
import schedule from './schedule';
import stories from './stories';

const rootReducer = combineReducers({
  app,
  downloads,
  matchFilter,
  schedule,
  stories,
});

export default rootReducer;
