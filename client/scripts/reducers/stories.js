/* eslint-disable require-jsdoc */

import { storiesFetcher, storyFetcher } from '../actions/stories';

const INITIAL_STATE = {
  stories: { items: null, error: null, loading: false },
  activeStory: { story: null, error: null, loading: false },
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case storiesFetcher.fetchAction:
      return {
        ...state,
        stories: { items: null, error: null, loading: true }
      };
    case storiesFetcher.successAction:
      return {
        ...state,
        stories: { items: action.payload, error: null, loading: false }
      };
    case storiesFetcher.failureAction:
      return {
        ...state,
        stories: { items: null, error: { message: action.payload.message }, loading: false }
      };
    case storiesFetcher.resetAction:
      return {
        ...state,
        stories: INITIAL_STATE.stories,
      };
    case storyFetcher.fetchAction:
      return {
        ...state,
        activeStory: { story: null, error: null, loading: true }
      };
    case storyFetcher.successAction:
      return {
        ...state,
        activeStory: { story: action.payload, error: null, loading: false }
      };
    case storyFetcher.failureAction:
      return {
        ...state,
        activeStory: { story: null, error: { message: action.payload.message }, loading: false }
      };
    case storyFetcher.resetAction:
      return {
        ...state,
        activeStory: INITIAL_STATE.activeStory,
      };
    default:
      return state;
  }
}
