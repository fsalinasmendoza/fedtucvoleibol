/* eslint-disable require-jsdoc */

import { matchFetcher } from '../../actions/schedule';

const INITIAL_STATE = { items: null, error: null, loading: false };

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case matchFetcher.fetchAction:
      return { items: null, error: null, loading: true };
    case matchFetcher.successAction:
      return { items: action.payload, error: null, loading: false };
    case matchFetcher.failureAction:
      return { items: null, error: { message: action.payload.message }, loading: false };
    case matchFetcher.resetAction:
      return INITIAL_STATE;
    default:
      return state;
  }
}
