/* eslint-disable require-jsdoc */

import { tournamentFetcher } from '../../actions/schedule';

const INITIAL_STATE = { tournament: null, error: null, loading: false };

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case tournamentFetcher.fetchAction:
      return { tournament: null, error: null, loading: true };
    case tournamentFetcher.successAction:
      return { tournament: action.payload, error: null, loading: false };
    case tournamentFetcher.failureAction:
      return { tournament: null, error: { message: action.payload.message }, loading: false };
    case tournamentFetcher.resetAction:
      return INITIAL_STATE;
    default:
      return state;
  }
}
