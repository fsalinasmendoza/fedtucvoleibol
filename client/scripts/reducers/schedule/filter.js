/* eslint-disable require-jsdoc */

import {
  SET_AGE_GROUP_FILTER,
  SET_BRANCH_FILTER,
  SET_PHASE_FILTER,
  SET_TEAM_FILTER,
} from '../../actions/schedule';

const INITIAL_STATE = {
  ageGroup: undefined,
  branch: undefined,
  phase: undefined,
  team: undefined,
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_AGE_GROUP_FILTER:
      if (state.ageGroup !== action.filter) {
        return {
          ...state,
          ageGroup: action.filter
        };
      }
      return state;
    case SET_BRANCH_FILTER:
      if (state.branch !== action.filter) {
        return {
          ...state,
          branch: action.filter
        };
      }
      return state;
    case SET_PHASE_FILTER:
      if (state.phase !== action.filter) {
        return {
          ...state,
          phase: action.filter
        };
      }
      return state;
    case SET_TEAM_FILTER:
      if (state.team !== action.filter) {
        return {
          ...state,
          team: action.filter
        };
      }
      return state;
    default:
      return state;
  }
}
