/* eslint-disable require-jsdoc */

import { combineReducers } from 'redux';

import filter from './schedule/filter';
import matches from './schedule/matches';
import tournament from './schedule/tournament';

const schedule = combineReducers({ filter, matches, tournament });

export default schedule;
