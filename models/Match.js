const keystone = require('keystone');

const Types = keystone.Field.Types;

const Match = new keystone.List('Match', {
  label: 'Partidos',
  singular: 'Partido',
  plural: 'Partidos',
  map: { name: 'title' },
  autokey: { path: 'slug', from: 'title date', unique: true },
  track: true,
});

/**
  * Devuelve el valor de 'datetime' convertido a una cadena que sólo contiene la fecha en formato
  * MM-DD-YY.
  */
function getDate() {
  return this._.datetime.format();
}

/**
  * Devuelve el valor de 'datetime' convertido a una cadena que sólo contiene la hora en formato
  * h:mm.
  */
function getTime() {
  return this._.datetime.format('H:mm');
}

Match.add({
  home: {
    label: 'Local',
    type: Types.Relationship,
    ref: 'Team',
    index: true,
    required: true,
    initial: true,
    filters: { enabled: true }
  },
  visit: {
    label: 'Visitante',
    type: Types.Relationship,
    ref: 'Team',
    index: true,
    required: true,
    initial: true,
    filters: { enabled: true }
  },
  ageGroup: {
    label: 'Categoría',
    type: Types.Relationship,
    ref: 'AgeGroup',
    index: true,
    required: true,
    initial: true,
    filters: { enabled: true }
  },
  branch: {
    label: 'Rama',
    type: Types.Select,
    options: ['Masculino', 'Femenino'],
    emptyOption: false,
    required: true,
    initial: true
  },
  tournament: {
    label: 'Torneo',
    type: Types.Relationship,
    ref: 'Tournament',
    index: true,
    required: true,
    initial: true,
    filters: { isActive: true },
  },
  phase: {
    label: 'Fase',
    type: Types.Relationship,
    ref: 'Phase',
    index: true,
    required: true,
    initial: true,
    dependsOn: { tournament: true },
    filters: {
      tournament: ':tournament',
      enabled: true
    },
  },
  stadium: {
    label: 'Cancha',
    type: Types.Relationship,
    ref: 'Team',
    required: true,
    initial: true,
    filters: { enabled: true }
  },
  datetime: {
    label: 'Fecha y hora',
    type: Types.Datetime,
    index: true,
    required: true,
    initial: true
  },
  time: {
    label: 'Hora',
    type: String,
    watch: 'datetime',
    value: getTime,
    hidden: true
  },
  date: {
    label: 'Fecha',
    type: String,
    watch: 'datetime',
    value: getDate,
    hidden: true
  },
  firstReferee: {
    label: 'Primer árbitro',
    type: Types.Relationship,
    ref: 'Referee',
    filters: { enabled: true }
  },
  secondReferee: {
    label: 'Segundo árbitro',
    type: Types.Relationship,
    ref: 'Referee',
    filters: { enabled: true }
  },
  scorekeeper: {
    label: 'Planillero',
    type: Types.Relationship,
    ref: 'Scorekeeper',
    filters: { enabled: true }
  },
  homeScore: {
    label: 'Resultado local',
    type: Types.Number,
  },
  visitScore: {
    label: 'Resultado visitante',
    type: Types.Number,
  },
  title: {
    type: String,
    hidden: true,
    noedit: true,
    initial: false,
    default: ''
  },
});

Match.schema.pre('save', function updateTitle(next, done) {
  if (
    this.isModified('home') ||
    this.isModified('visit') ||
    this.isModified('ageGroup') ||
    this.isModified('branch')
  ) {
    const self = this;

    keystone.list('Team').model.findById(self.home).exec((homeErr, home) => {
      if (homeErr) {
        done(homeErr);
      } else if (home == null) {
        done(new Error('No se pudo encontrar el equipo local.'));
      } else {
        keystone.list('Team').model.findById(self.visit).exec((visitErr, visit) => {
          if (visitErr) {
            done(visitErr);
          } else if (visit == null) {
            done(new Error('No se pudo encontrar el equipo visitante.'));
          } else {
            keystone.list('AgeGroup').model.findById(self.ageGroup).exec((ageGroupErr, ageGroup) => {
              if (ageGroupErr) {
                done(ageGroupErr);
              } else if (ageGroup == null) {
                done(new Error('No se pudo encontrar la categoría.'));
              } else {
                self.title = `${self.branch} ${ageGroup.name} - ${home.name} vs ${visit.name}`;
                next();
              }
            });
          }
        });
      }
    });
  } else {
    next();
  }
});

Match.defaultColumns = 'ageGroup, date, time, home, visit';
Match.register();
