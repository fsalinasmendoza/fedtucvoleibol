const keystone = require('keystone');
const winston = require('winston');

const Types = keystone.Field.Types;

const Scorekeeper = new keystone.List('Scorekeeper', {
  label: 'Planilleros',
  singular: 'Planillero',
  plural: 'Planilleros',
  map: { name: 'name' },
  autokey: { path: 'slug', from: 'name', unique: true },
  track: true,
});

Scorekeeper.add({
  name: { label: 'Nombre', type: Types.Name, required: true, initial: true },
  enabled: { label: 'Habilitado', type: Boolean, default: true }
});

Scorekeeper.schema.pre('remove', function checkRelationships(next) {
  const self = this;

  const Match = keystone.list('Match');

  Match.model.find({ scorekeeper: self._id }).lean().limit(1).exec((err, matches) => {
    if (err) {
      winston.error(err.message);
      next(new Error('Error interno.'));
    } else if (matches != null && matches.length !== 0) {
      next(new Error('El planillero se encuentra asociado a un partido.'));
    } else {
      next();
    }
  });
});

Scorekeeper.defaultColumns = 'name, enabled';
Scorekeeper.register();
