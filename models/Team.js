const keystone = require('keystone');
const winston = require('winston');

const Types = keystone.Field.Types;

const Team = new keystone.List('Team', {
  label: 'Equipos',
  singular: 'Equipo',
  plural: 'Equipos',
  map: { name: 'name' },
  autokey: { path: 'slug', from: 'name', unique: true },
  track: true,
});

/**
  * Acomoda el logo para entrar en un área de 80x80
  */
function transformLogo() {
  return this._.image.pad(80, 80, { background: '#00000000' });
}

Team.add({
  name: { label: 'Nombre', type: String, required: true },
  image: { label: 'Logo', type: Types.CloudinaryImage },
  logo: {
    type: String,
    hidden: true,
    watch: 'image',
    value: transformLogo
  },
  address: {
    label: 'Dirección',
    type: Types.Location,
    defaults: {
      country: 'Argentina',
      state: 'Tucumán'
    }
  },
  enabled: { label: 'Habilitado', type: Boolean, default: true }
});

Team.schema.pre('remove', function checkRelationships(next) {
  const self = this;

  const Match = keystone.list('Match');

  Match.model
    .find({ $or: [{ home: self._id }, { visit: self._id }] })
    .lean()
    .limit(1)
    .exec((err, matches) => {
      if (err) {
        winston.error(err.message);
        next(new Error('Error interno.'));
      } else if (matches != null && matches.length !== 0) {
        next(new Error('El equipo se encuentra asociado a un partido.'));
      } else {
        next();
      }
    }
  );
});

Team.defaultColumns = 'name, enabled';
Team.register();
