const keystone = require('keystone');

const Types = keystone.Field.Types;

const Download = new keystone.List('Download', {
  label: 'Descargas',
  singular: 'Descarga',
  plural: 'Descargas',
  map: { name: 'name' },
  autokey: { path: 'slug', from: 'name', unique: true },
  defaultSort: '-publishedAt',
  track: true,
});

Download.add({
  name: { label: 'Nombre', type: String, required: true },
  category: {
    label: 'Categoría',
    type: Types.Select,
    required: true,
    initial: true,
    options: [
      { value: 'regulations', label: 'reglamentos' },
      { value: 'forms', label: 'formularios' },
      { value: 'resolutions', label: 'resoluciones' },
      { value: 'resources', label: 'recursos' }
    ],
    index: true
  },
  file: {
    label: 'Archivo',
    type: Types.LocalFile,
    dest: 'public/download',
    prefix: 'download/',
    filename: (item, file) => (`${item.id}.${file.extension}`),
    format: (item, file) => (
      `<a href="/download/${file.filename}">${file.originalname}</a>`
    ),
    allowedTypes: [
      'application/pdf',
      'application/vnd.ms-excel',
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    ]
  }
});

Download.defaultColumns = 'name, category|15%, file|25%';
Download.register();
