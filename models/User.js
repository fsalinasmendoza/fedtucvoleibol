const keystone = require('keystone');
const winston = require('winston');
const transform = require('model-transform');

const Types = keystone.Field.Types;

const User = new keystone.List(
  'User', {
    label: 'Usuarios',
    singular: 'Usuario',
    plural: 'Usuarios',
    nodelete: true,
    track: true,
  }
);

User.add({
  name: { label: 'Nombre', type: Types.Name, required: true, index: true },
  email: { type: Types.Email, initial: true, required: true, index: true },
  password: {
    label: 'Contraseña',
    type: Types.Password,
    initial: true,
    required: true
  }
}, 'Permisos', {
  isAdmin: { label: 'Es administrador', type: Boolean, index: true }
});

User.schema.pre('remove', function checkRelationships(next) {
  winston.error(`Se intentó borrar el usuario ${this._id}`);
  next(new Error('No se puede borrar usuarios.'));
});

// Provide access to Keystone
User.schema.virtual('canAccessKeystone').get(function isAdmin() {
  return this.isAdmin;
});

transform.toJSON(User);

User.defaultColumns = 'name, email, isAdmin';
User.register();
