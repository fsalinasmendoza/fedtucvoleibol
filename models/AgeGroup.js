const keystone = require('keystone');
const winston = require('winston');

const AgeGroup = new keystone.List('AgeGroup', {
  label: 'Categorías',
  singular: 'Categoría',
  plural: 'Categorías',
  map: { name: 'name' },
  autokey: { path: 'slug', from: 'name', unique: true },
  sortable: true,
  track: true,
});

AgeGroup.add({
  name: { label: 'Nombre', type: String, required: true, initial: true },
  enabled: { label: 'Habilitado', type: Boolean, default: true }
});

AgeGroup.schema.pre('remove', function checkRelationships(next) {
  const self = this;

  const Match = keystone.list('Match');

  Match.model.find({ ageGroup: self._id }).lean().limit(1).exec((err, matches) => {
    if (err) {
      winston.error(err.message);
      next(new Error('Error interno.'));
    } else if (matches != null && matches.length !== 0) {
      next(new Error('La categoría se encuentra asociada a un partido.'));
    } else {
      next();
    }
  });
});

AgeGroup.defaultColumns = 'name, enabled';
AgeGroup.register();
