const keystone = require('keystone');

const Types = keystone.Field.Types;

const Story = new keystone.List('Story', {
  label: 'Noticias',
  singular: 'Noticia',
  plural: 'Noticias',
  map: { name: 'title' },
  autokey: { path: 'slug', from: 'title', unique: true },
  defaultSort: '-publishedAt',
  track: true,
});

/**
  * Acomoda el logo para darle la relación de aspecto apropiada.
  */
function transformPhoto() {
  return this._.image.src({ aspect_ratio: '16:9', crop: 'crop' });
}

/**
  * Devuelve el contenido de la notica en formato HTML pero habiendo eliminado los tags <code />
  * y <pre />
  */
function removeCodeAndPreTags() {
  const brief = this.content.html;
  return brief ? brief.replace(/<code>|<\/code>|<pre>|<\/pre>/gi, '') : brief;
}

Story.add({
  title: { label: 'Título', type: String, required: true },
  state: {
    label: 'Estado',
    type: Types.Select,
    options: [
      { value: 'draft', label: 'borrador' },
      { value: 'published', label: 'publicado' },
      { value: 'archived', label: 'archivado' }
    ],
    default: 'draft',
    index: true
  },
  publishedAt: {
    label: 'Fecha de publicación',
    type: Types.Datetime,
    index: true,
    watch: { state: 'published' },
    value: Date.now,
    hidden: true
  },
  image: { label: 'Foto', type: Types.CloudinaryImage },
  photo: {
    type: String,
    hidden: true,
    watch: 'image',
    value: transformPhoto
  },
  content: {
    label: 'Contenido',
    type: Types.Markdown,
    height: 400,
    toolbarOptions: { hiddenButtons: 'cmdImage' }
  },
  brief: {
    label: 'Resumen',
    type: String,
    watch: 'content',
    value: removeCodeAndPreTags,
    hidden: true,
  },
  attachment: {
    label: 'Archivo adjunto',
    type: Types.LocalFile,
    dest: 'public/download',
    prefix: 'download/',
    filename: (item, file) => (`${item.id}.${file.extension}`),
    format: (item, file) => (
      `<a href="/download/${file.filename}">${file.originalname}</a>`
    ),
    allowedTypes: ['application/pdf']
  }
});

Story.defaultColumns = 'title, state|15%, category|15%, publishedAt|15%';
Story.register();
