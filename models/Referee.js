const keystone = require('keystone');
const winston = require('winston');

const Types = keystone.Field.Types;

const Referee = new keystone.List('Referee', {
  label: 'Árbitros',
  singular: 'Árbitro',
  plural: 'Árbitros',
  map: { name: 'name' },
  autokey: { path: 'slug', from: 'name', unique: true },
  track: true,
});

Referee.add({
  name: { label: 'Nombre', type: Types.Name, required: true, initial: true },
  enabled: { label: 'Habilitado', type: Boolean, default: true }
});

Referee.schema.pre('remove', function checkRelationships(next) {
  const self = this;

  const Match = keystone.list('Match');

  Match.model
    .find({ $or: [{ firstReferee: self._id }, { secondReferee: self._id }] })
    .lean()
    .limit(1)
    .exec((err, matches) => {
      if (err) {
        winston.error(err.message);
        next(new Error('Error interno.'));
      } else if (matches != null && matches.length !== 0) {
        next(new Error('El árbitro se encuentra asociado a un partido.'));
      } else {
        next();
      }
    }
  );
});

Referee.defaultColumns = 'name, enabled';
Referee.register();
