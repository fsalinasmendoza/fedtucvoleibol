const keystone = require('keystone');
const winston = require('winston');

const Tournament = new keystone.List('Tournament', {
  label: 'Torneos',
  singular: 'Torneo',
  plural: 'Torneos',
  map: { name: 'name' },
  autokey: { path: 'slug', from: 'name', unique: true },
  track: true,
});

Tournament.add({
  name: { label: 'Nombre', type: String, required: true, initial: true },
  isActive: {
    label: 'Activo',
    type: Boolean,
    index: true,
    initial: true
  }
});

Tournament.schema.pre('save', function updatePhaseStatus(next, done) {
  const self = this;

  if (self.isModified('isActive') && self.isActive === true) {
    Tournament.model
      .update({ _id: { $ne: self._id } }, { isActive: false }, { multi: true }, (err) => {
        if (err) {
          done(err);
        } else {
          next();
        }
      });
  } else {
    next();
  }
});

Tournament.schema.pre('remove', function checkRelationships(next) {
  const self = this;

  const Phase = keystone.list('Phase');

  Phase.model
    .find({ tournament: self._id })
    .lean()
    .limit(1)
    .exec((err, phases) => {
      if (err) {
        winston.error(err.message);
        next(new Error('Error interno.'));
      } else if (phases != null && phases.length !== 0) {
        next(new Error('El torneo se encuentra asociado a una fase.'));
      } else {
        next();
      }
    }
  );
});

Tournament.relationship({ path: 'phases', ref: 'Phase', refPath: 'tournament' });
Tournament.defaultColumns = 'name, isActive|15%';
Tournament.register();
