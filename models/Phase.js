const keystone = require('keystone');
const winston = require('winston');

const Types = keystone.Field.Types;

const Phase = new keystone.List('Phase', {
  label: 'Fases',
  singular: 'Fase',
  plural: 'Fases',
  map: { name: 'name' },
  autokey: { path: 'slug', from: 'name tournament', unique: true },
  drilldown: 'tournament',
  sortable: true,
  track: true,
});

Phase.add({
  name: { label: 'Nombre', type: String, required: true },
  tournament: {
    label: 'Campeonato',
    type: Types.Relationship,
    ref: 'Tournament',
    index: true,
    required: true,
    initial: true
  },
  enabled: { label: 'Habilitada', type: Boolean, default: true }
});

Phase.schema.pre('remove', function checkRelationships(next) {
  const self = this;

  const Match = keystone.list('Match');

  Match.model.find({ phase: self._id }).lean().limit(1).exec((err, matches) => {
    if (err) {
      winston.error(err.message);
      next(new Error('Error interno.'));
    } else if (matches != null && matches.length !== 0) {
      next(new Error('La fase se encuentra asociada a un partido.'));
    } else {
      next();
    }
  });
});

Phase.relationship({ path: 'matches', ref: 'Match', refPath: 'phase' });
Phase.defaultColumns = 'number|15%, tournament, enabled';
Phase.register();
